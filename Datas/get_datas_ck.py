# Fichier à supprimer dans le futur

import os
from glob import glob
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from Datas.thread_safe import threadsafe_generator
import cv2 as cv

# @TODO prévoir une meilleure gestions des paramètres d'entrées ?
# os.sep


# @TODO mettre dans un file config:
# data_dir = os.path.join('Datas', 'DB', 'CK-cropped-images')
# all_files = os.path.join(data_dir, 'all_files', 'train')
# neutral_files = os.path.join(data_dir, 'neutral_files', 'train')
# peak_files = os.path.join(data_dir, 'peak_files')
# Paramètres à mettre dans un fichier de config:
# defaults_params = {'img_width': 66,
#                   'img_height': 66,
#                   'img_width_output': 64,
#                   'img_height_output': 64,
#                   'batch_size': 28}

# Parametres
# @TODO Dictionnaire plus propre
img_width = 66
img_height = 66
img_width_output = 64
img_height_output = 64
batch_size = 28

print('os name', os.name)
# Construction  de la liste des emplacements des données
# Distinction en fonction de l'OS
if os.name != "nt":
    data_dir = 'Datas/DB/CK-images/cohn-kanade-images/'
    # data_dir_test = 'DB/CK-images/cohn-kanade-images/'
    # path des visages neutres (premiere frame):
    data_files_neutral = glob(os.path.join(data_dir, 'S*/*/*01.png'))
    data_files_neutral += data_files_neutral
    # path des visages avec expression (derniere frame)
    all_folders = glob(os.path.join(data_dir, '*/*'))
    cv_path = '/home/ugop/Bureau/onemood/Datas/haarcascade_frontalface_alt.xml'
    all_files = glob(os.path.join(data_dir, '*/*/*.png'))
else:
    data_dir = 'Datas\\DB\\CK-images\\cohn-kanade-images\\'
    data_dir_test = 'DB\\CK-images\\cohn-kanade-images\\'
    # path des visages neutres (premiere frame):
    data_files_neutral = glob(os.path.join(data_dir, 'S*\\*\\*01.png'))
    # on le double pour avoir le même nombre d'images neutres et expressives
    data_files_neutral += data_files_neutral
    # path des visages avec expression
    all_folders = glob(os.path.join(data_dir, '*\\*'))
    cv_path = 'C:\\Users\\m.adolphe\\Desktop\\Stage\\onemood\\Datas\\haarcascade_frontalface_alt.xml'
    all_files = glob(os.path.join(data_dir, '*\\*\\*.png'))
# Récupération des paths des 2 dernières images de chaque séquence
data_files_peak = []
data_files_peak2 = []
for folder_path in all_folders:
    temp = glob(os.path.join(folder_path, '*.png'))
    peak_file = temp[-1]
    peak_file_2 = temp[-2]
    data_files_peak.append(peak_file)
    data_files_peak2.append(peak_file_2)
# On stack une liste sur l'autre pour facilier l'opération similaire sur les données neutres
data_files_peak += data_files_peak2


def get_img(img, out=False):
    """
    Renvoie une image selon les dimensions définies par le module
    :param img:
    :param out:
    :return:
    """
    open_img = cv.imread(img)
    face_cascade = cv.CascadeClassifier(cv_path)
    # Distinction pour une utilisation en sortie ou en entrée (pas les mêmes dimensions)
    if out:
        # Si on doit traiter une image RVB:
        if (np.array(open_img)).ndim > 2:
            # Conversion en niveau de gris openCV
            open_img = cv.cvtColor(open_img, cv.COLOR_BGR2GRAY)

        # Détection du visage
        faces = face_cascade.detectMultiScale(open_img, scaleFactor=1.3, minNeighbors=5,
                                              flags=cv.CASCADE_SCALE_IMAGE)
        for (x, y, w, h) in faces:
            # On rogne sur la zone du visage d'intérêt
            open_img = open_img[y:y + h, x:x + w]
        resized_img = (Image.fromarray(open_img)).resize((img_width_output, img_height_output), Image.HAMMING)
        return np.array(resized_img)
    else:
        if (np.array(open_img)).ndim > 2:
            open_img = cv.cvtColor(open_img, cv.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(open_img, scaleFactor=1.3, minNeighbors=5)
        for (x, y, w, h) in faces:
            open_img = open_img[y:y+h, x:x+w]
        resized_img = (Image.fromarray(open_img)).resize((img_width, img_height), Image.HAMMING)
        return np.array(resized_img)


def get_nbr_steps(autoencoder2=False):
    """
    Recupere le nombre de batch par epoch
    :return:
    """
    if autoencoder2:
        return len(all_files) // batch_size
    else:
        if len(data_files_neutral) == len(data_files_peak):
            return len(data_files_neutral)//batch_size
        else:
            print('error')
            # logs raise 'error'

# On utilise un décorateur pour modifier le comportement par défaut de la fonction génératrice
# Le comportement supplémentaire apporté est la gestion du thread pour l'execution d'une génération d'un batch
@threadsafe_generator
def get_batches(autoencoder2=False):
    """
    Generator qui capable de renvoyer un batch pour autoencoder 1 ou 2
    mutualiser et expliquer
    """
    if autoencoder2:
        # Si on yield un batch pour l'autoencoder 2 il faut des entrées et des targets qui soient exactement les mêmes
        current_index = 0
        while current_index <= len(all_files):
            batch = np.array([get_img(img) for img in all_files[current_index:current_index+batch_size]])
            batch_out = np.array([get_img(img, out=True) for img in all_files[current_index:current_index+batch_size]])
            batch = (batch/255).astype(np.float32)
            batch_out = (batch_out/255).astype(np.float32)
            current_index = (current_index + batch_size) % (len(all_files)-batch_size)
            print("current index = ", current_index)
            yield batch.reshape((batch_size, img_width, img_height, 1)), batch_out.reshape((batch_size,
                                                                                            img_width_output,
                                                                                            img_height_output,
                                                                                            1))
    else:
        # Si on yield un batch pour l'autoencoder 1 il faut des entrées avec émotion et des sorties neutres
        current_index = 0
        while current_index <= len(data_files_neutral):
            batch_neutral = np.array([get_img(img, out=True) for img in data_files_neutral[current_index:
                                                                                           current_index+batch_size]])
            batch_peak = np.array([get_img(img) for img in data_files_peak[current_index:current_index+batch_size]])
            batch_peak = (batch_peak/255).astype(np.float32)
            batch_neutral = (batch_neutral/255).astype(np.float32)
            current_index = (current_index + batch_size) % (len(data_files_neutral)-batch_size)
            yield batch_peak.reshape((batch_size, img_width, img_height, 1)), batch_neutral.reshape((batch_size,
                                                                                                     img_width_output,
                                                                                                     img_height_output,
                                                                                                     1))


def get_imgs_tests(nbre=3):
    """
    Renvoie simplement des images pour le test
    :param nbre:
    :return:
    """
    imgs_tests = [get_img(img) for img in data_files_peak[:nbre]]
    return np.array(imgs_tests).reshape(nbre, img_width, img_height, 1)


if __name__ == '__main__':

    def test_size_paths():
        print(len(all_files))
    # test_size_paths()

    def test_paths():
        print(len(data_files_peak))
        print(len(data_files_neutral))
    # test_paths()

    def test_crop():
        img = Image.open(data_files_neutral[0])
        plt.figure("Original")
        plt.imshow(img)
        plt.show(block=False)
        plt.figure("cropper")
        img = get_img(data_files_neutral[0], True)
        plt.imshow(img)
        plt.show(2)
    # test_crop()

    def test_batch():
        print("Nombre de batch par epoch auto 1: ", get_nbr_steps())
        print("Nombre de batch par epoch auto 2: ", get_nbr_steps(autoencoder2=True))
        print("Nombre de fichiers neutres: ", len(data_files_neutral))
        print("Nombre de fichiers emotions: ", len(data_files_peak))
        print("Nombre total de fichiers: ", len(all_files))
        # Test pour l'autoencoder1
        gen_test = get_batches(autoencoder2=False)
        for emotion, neutre in gen_test:
            plt.figure("Neutral face")
            plt.imshow(neutre[0, :, :, 0], cmap='gray')
            plt.show(block=False)
            plt.figure("Emotion face")
            plt.imshow(emotion[0, :, :, 0], cmap='gray')
            plt.show()
        # Test pour l'autoencoder
        gen_test = get_batches(autoencoder2=True)
        for img, same_img in gen_test:
            plt.figure("Image")
            plt.imshow(img[0, :, :, 0], cmap='gray')
            plt.show(block=False)
            plt.figure("La meme image")
            plt.imshow(same_img[0, :, :, 0], cmap='gray')
            plt.show()
    # test_batch()

    def test_generator():
        print("Nombre de batch par epoch auto 2: ", get_nbr_steps(autoencoder2=True))
        print("Nombre total de fichiers: ", len(all_files))
        i = 0
        gen_test = get_batches(autoencoder2=True)
        for batch1, batch2 in gen_test:
            i += 1
            print("batch1 shape =", batch1.shape)
            print("batch2 shape =", batch2.shape)
            print(i)
    test_generator()