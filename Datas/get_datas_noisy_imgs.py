from glob import glob
from PIL import Image
import numpy as np
import os

data_dir = 'Datas\\visages_dataset'
img_width = 66
img_height = 66
img_width_output = 64
img_height_output = 64
data_files = glob(os.path.join(data_dir, '*.jpg'))
batch_size = 64
data_files=data_files[:1024]

def get_input_img(img):
    open_img = Image.open(img)
    if (np.array(open_img)).ndim > 2:
        open_img = open_img.convert('L')
    resized_img = open_img.resize((img_width, img_height), Image.HAMMING)
    return np.array(resized_img)


def get_output_img(img):
    """méthode factorisable avec la première"""
    open_img = Image.open(img)
    if (np.array(open_img)).ndim > 2:
        open_img = open_img.convert('L')
    resized_img = open_img.resize((img_width_output, img_height_output), Image.HAMMING)
    return np.array(resized_img)


def get_nbr_steps():
    return len(data_files)//batch_size


def get_batches():
    current_index = 0
    while current_index <= len(data_files):
        batch = np.array([get_output_img(img) for img in data_files[current_index:current_index+batch_size]])
        batch = (batch/255).astype(np.float32)
        noisy_batch = np.array([get_input_img(img) for img in data_files[current_index:current_index+batch_size]])
        noisy_batch = (noisy_batch / 255).astype(np.float32)
        current_index = (current_index + batch_size) % (len(data_files)-batch_size)
        noise = np.random.normal(0, 0.1, (batch_size, img_width, img_height))
        noisy_batch = noisy_batch + noise
        noisy_batch = noisy_batch / np.max(noisy_batch)
        yield noisy_batch.reshape((batch_size, img_height, img_height, 1)), batch.reshape((batch_size, img_height_output, img_height_output, 1))


if __name__ == '__main__':
    i = 1
    test = data_files = glob(os.path.join('visages_dataset', '*.jpg'))
    print(len(data_files))
    batches = get_batches()
    for n_batch, batch in batches:
        if n_batch.shape != batch.shape:
            print(n_batch.shape, batch.shape)

    while i <= len(data_files)-1:
        img = Image.open(data_files[i])
        if (np.array(img)).ndim > 2:
            img = img.convert('L')
            print((np.array(img)).shape, i)
        i = i+1
