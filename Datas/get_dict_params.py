import os

# Images de references pour initialisation (et tests)
dir_path_peak = os.path.join('Datas', 'DB', 'CK-cropped-images', 'peak_files')
dir_path_label = os.path.join('Datas', 'DB', 'CK-cropped-images', 'label_files')
dir_path_neutral_label = os.path.join('Datas', 'DB', 'CK-cropped-images', 'label_neutral_ref_files')
dir_path_neutral = os.path.join('Datas', 'DB', 'CK-cropped-images', 'neutral_files')
dir_path_all = os.path.join('Datas', 'DB', 'CK-cropped-images', 'all_files', 'train')
params_test = {
    'dir_path': dir_path_peak,
    'batch_size': 3,
    'img_height': 64,
    'img_width': 64,
    'shuffle': False,
    'class_mode': None,
    'validation_split': 0.2
}
params_disc = {
    'dir_path': dir_path_label,
    'batch_size': 32,
    'img_height': 64,
    'img_width': 64,
    'shuffle': True,
    'class_mode': 'categorical',
    'validation_split': 0.2
}
params_autoencoder1_in = {
    'dir_path': dir_path_peak,
    'batch_size': 32,
    'img_height': 64,
    'img_width': 64,
    'shuffle': False,
    'mode': None,
    'validation_split': 0
}
params_autoencoder1_out = {
    'dir_path': dir_path_neutral,
    'batch_size': 32,
    'img_height': 64,
    'img_width': 64,
    'shuffle': False,
    'mode': None,
    'validation_split': 0
}
params_autoencoder2 = {
    'dir_path': dir_path_all,
    'batch_size': 32,
    'img_height': 64,
    'img_width': 64,
    'shuffle': True,
    'class_mode': 'input',
    'validation_split': 0
}
params_in_darkmood = {
    'dir_path': dir_path_label,
    'batch_size': 16,
    'img_height': 64,
    'img_width': 64,
    'shuffle': False,
    'mode': 'categorical',
    'validation_split': 0
}
params_out_darkmood = {
    'dir_path': dir_path_neutral_label,
    'batch_size': 16,
    'img_height': 64,
    'img_width': 64,
    'shuffle': False,
    'mode': None,
    'validation_split': 0
}
params_emotion = {
    '0': "Enervement",
    '1': "Neutral",
    '2': "Dégout",
    '3': "Peur",
    '4': "Joie",
    '5': "Tristesse",
    '6': "Surprise"}