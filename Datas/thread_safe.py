# https://gist.github.com/platdrag/e755f3947552804c42633a99ffd325d4
# Décorateur permettant de corriger le probleme des générateurs qui ne sont pas thread-safe

import threading


class ThreadSafeIter:
    """
    Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, it):
        self.it = it
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return next(self.it)


# On définit ici le décorateur, celui-ci initialise et retourne un nouvel objet générateur
# Il retourne comme prévu la définition d'une méthode (le nouveau comportement de notre méthode à modifier)
def threadsafe_generator(f):
    """
    A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return ThreadSafeIter(f(*a, **kw))
    return g
