import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.losses import mean_squared_error
import numpy as np


if __name__ == '__main__':
    X = np.array([np.random.normal(0, 0.5, 10) for i in range(1000)])
    Y = np.array([np.random.normal(0, 0.5, 1) for i in range(1000)])
    print(X.shape)
    print(Y.shape)
    sous_model_1 = Sequential()
    sous_model_2 = Sequential()
    sous_model_1.add(Dense(100, input_shape=(10,)))
    sous_model_1.add(Dense(100))
    sous_model_1.summary()
    sous_model_2.add(Dense(100))
    sous_model_2.add(Dense(1))
    model = Sequential()
    model.add(sous_model_1)
    model.add(sous_model_2)
    model.summary()
    model.compile(optimizer='adam', loss=mean_squared_error)
    w = []
    for layer in sous_model_1.layers:
        w.append(layer.get_weights())
    print(np.array(w)[0, 0])
    model.fit(X, Y, epochs=1000, batch_size=100)
    # Apres apprentissage du modele complet, les premieres couches sont bien modifiées (à l'exterieur) :
    w_after = []
    for layer in sous_model_1.layers:
        w_after.append(layer.get_weights())
    first_w_after = np.array(w_after)[0, 0]
    res = np.array(w)[0, 0] == np.array(w_after)[0, 0]
    print(res)
    # Si l'on entraîne juste le sous modèle :
    sous_model_1.compile(optimizer='adam', loss=mean_squared_error)
    Y2 = np.array(np.array([np.random.normal(0, 0.5, 100) for i in range(1000)]))
    sous_model_1.fit(X, Y2)
    m = []
    for layer in model.layers:
        m.append(layer.get_weights())
    first_m = np.array(m)[0, 0]
    print(first_m == first_w_after)
    model.fit(X, Y, epochs=1000, batch_size=100)
