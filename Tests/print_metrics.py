import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    total_loss = np.load("losses.npy")
    total_acc = np.load("accuracy.npy")
    # Moyenne tous les 17

    tot_loss = total_loss[0, :]
    L_gen = total_loss[1, :]
    L_recons_ref = total_loss[2, :]
    L_recons_input = total_loss[3, :]
    L_contr = total_loss[4, :]
    L_class = total_loss[5, :]

    avg_acc = []
    avg_tot_loss = []
    avg_L_gen = []
    avg_L_recons_ref = []
    avg_L_recons_input = []
    avg_L_contr = []
    avg_L_class = []

    def moy_acc():
        i = 0
        while i < 25347:
            avg_acc.append(np.mean(total_acc[i:i + 17]))
            i += 17
        return
    moy_acc()
    plt.figure()
    plt.plot(avg_acc)
    plt.title('Précision moyenne sur 1500 epoch')
    plt.show(block=False)

    def moy_loss():
        i = 0
        while i < 25347:
            avg_tot_loss.append(np.mean(tot_loss[i:i+17]))
            avg_L_gen.append(np.mean(L_gen[i:i+17]))
            avg_L_recons_ref.append(np.mean(L_recons_ref[i:i+17]))
            avg_L_recons_input.append(np.mean(L_recons_input[i:i+17]))
            avg_L_contr.append(np.mean(L_contr[i:i+17]))
            avg_L_class.append(np.mean(L_class[i:i+17]))
            i += 17
        return
    moy_loss()

    fig = plt.figure(figsize=(18, 10))
    fig.add_subplot(231)
    plt.title('Perte totale moyenne par epoch')
    plt.plot(avg_tot_loss)

    fig.add_subplot(232)
    plt.title('Perte moyenne par epoch de reconstruction sans émotion')
    plt.plot(avg_L_gen)

    fig.add_subplot(233)
    plt.title('Perte de contraste moyenne par epoch')
    plt.plot(avg_L_contr)

    fig.add_subplot(234)
    plt.title('Perte moyenne par epoch de reconstruction de l\'input')
    plt.plot(avg_L_recons_input)

    fig.add_subplot(235)
    plt.title('Perte moyenne par epoch de reconstruction de l\'image générée')
    plt.plot(avg_L_recons_ref)

    fig.add_subplot(236)
    plt.title('Perte moyenne par epoch de classification')
    plt.plot(avg_L_class)

    plt.show()