from keras.models import Sequential
from keras import backend as K
from keras.layers import Dense
import random
from keras.losses import mean_squared_error
import numpy as np

def custom_loss_gen(y_true, y_pred):
    """
    Fonction de perte (surtout) pour préentrainement de l'autoencoder1
    :param y_true:
    :param y_pred:
    :return:
    """
    # Théorie ?? peut être à remplacer
    delta = y_true - y_pred
    loss = (1 / 2) * (K.sqrt(K.sum(K.square(delta))))
    print('prediction :', y_pred)
    print('verite :', y_true)
    print(loss)
    return loss


if __name__ == '__main__':
    # Création d'un autoencoder trés simple devant effectuer une soustraction de 1
    def test_loss():
        X = np.array([random.randint(0, 50) for i in range(1000)])
        Y = X - 1
        print(X[:10])
        print(Y[:10])
        print(X.shape, Y.shape)
        model = Sequential()
        model.add(Dense(100, input_shape=(1,)))
        model.add(Dense(1))
        model.summary()
        model.compile(optimizer='Adam', loss=custom_loss_gen, metrics=['acc'])
        model.fit(X, Y, epochs=100, steps_per_epoch=100, verbose=0)
        out = model.predict(np.array([1, 2, 3]))
        print(out)
    test_loss()
