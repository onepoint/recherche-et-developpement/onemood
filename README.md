# Projet onemood
La détection automatique des émotions est emplie de nombreux enjeux: apprentissage, sécurité, médecine, marketing... 
Ce projet tend à reproduire une architecture publiée par Zhang et son équipe en 2019 dans l'article "Deep Generative 
Contrastive Network" (https://arxiv.org/abs/1703.07140).
</br>
L'idée principale de l'étude est de comparer un visage expressif à une représentation générée de ce même 
visage (représentation sans émotion). L'intuition de l'équipe est que la production d'une image de référence
permettra une meilleure gestion de la problématique de variabilité inter et intra-sujet existante dans le domaine 
du FER (Face Expression Recognition).

# Installation

Requis: Python 3.5+ | Linux, Mac OS X, Windows

```sh
pip install pipenv
```
Puis dans le dossier du projet:  

```sh
pipenv install 
```
Le pipfile permettra l'installation de toutes les dépendances nécessaires à l'utilisation du projet. 
Puis pour exécuter des commandes dans cet environnement virtuel: 

```sh
pipenv shell
```

# Description de l'architecture:

L'architecture est d'abord constituée d'un modèle génératif capable de transformer une image d'entrée en une représentation
sans émotion de ce visage. L'article propose 3 méthodes de générations mais seule la méthode utilisant un autoencoder a été
utilisé ici. Cet autoencoder est basé sur l'utilisation de couches de convolutions, de pooling et de batch-normalization.
</br>
Le corpus utilisé pour l'apprentissage du modèle est la base de données CK+ (non disponible sur ce répertoire). Un pré-entraînement
de certaines parties de notre modèle a été effectué avec ce même dataset. 


