import os
import shutil


def raz_logs(filename):
    """
    Purge des logs

    :param filename:
    :return:
    """
    filepath = os.path.join('Save\\logs', filename)

    if os.path.exists(filepath):
        try:
            shutil.rmtree(filepath)
        except OSError:
            os.remove(filepath)

