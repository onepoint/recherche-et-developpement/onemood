# Script pour la "traduction" des modèles chargés en .h5 vers des modèles ne contenant que les poids des réseaux pré-
# entraînés

# Non utile pour l'apprentissage des modèles


from keras.models import Sequential, load_model, Model
from Networks.Autoencoders.encoder import Encoder
from Networks.Autoencoders.decoder import Decoder
from Networks.Discriminator.discriminator import Discriminator
from keras.layers import Input
from Datas.get_dict_params import *
from Datas.get_datas_ck_cropped import init_disc_generator, init_auto_generator, get_imgs_test
import matplotlib.pyplot as plt

if __name__ == '__main__':
    def translation(model, new):
        model = load_model(model)
        model.save_weights(new)
        return
    # translation('autoencoder2-1800-train', 'autoencoder2-1800-weights')

    def get_autoencoder1():
        inputs_autoencoder_1 = Input(shape=(64, 64, 1,))
        encoder_1_ref = Encoder((64, 64, 1), verbose=False).model
        encoder_1_ref.name = 'encoder_1_ref'
        output_encoder_1_ref = encoder_1_ref(inputs_autoencoder_1)

        decoder_1_ref = Decoder((8, 8, 96), verbose=False).model
        decoder_1_ref.name = 'decoder_1_ref'
        output_decoder_1_ref = decoder_1_ref(output_encoder_1_ref)
        autoencoder1 = Model(inputs=inputs_autoencoder_1, outputs=[output_decoder_1_ref])
        autoencoder1.load_weights('autoencoder1-1400-weights')
        [_, encoder1, decoder1] = [morceau for morceau in autoencoder1.layers]
        encoder1.save_weights('encoder1-1400-weights')
        decoder1.save_weights('decoder1-1400-weights')
        return autoencoder1

    def get_autoencoder2():
        inputs_autoencoder_2 = Input(shape=(64, 64, 1,))
        encoder_2_ref = Encoder((64, 64, 1), verbose=False).model
        encoder_2_ref.name = 'encoder_1_ref'
        output_encoder_2_ref = encoder_2_ref(inputs_autoencoder_2)

        decoder_2_ref = Decoder((8, 8, 96), verbose=False).model
        decoder_2_ref.name = 'decoder_1_ref'
        output_decoder_2_ref = decoder_2_ref(output_encoder_2_ref)
        autoencoder2 = Model(inputs=inputs_autoencoder_2, outputs=[output_decoder_2_ref])
        autoencoder2.load_weights('autoencoder2-1800-weights')
        [_, encoder2, decoder2] = [morceau for morceau in autoencoder2.layers]
        encoder2.save_weights('encoder2-1800-weights')
        decoder2.save_weights('decoder2-1800-weights')
        return autoencoder2

    def get_encoder():
        model = Sequential()
        encoder = Encoder((64, 64, 1), verbose=False)
        discriminator = Discriminator()
        model.add(encoder.model)
        model.add(discriminator.model)
        model.load_weights('baselineCNN-2987-weights')
        [encoder, classifieur] = [module for module in model.layers]
        encoder.summary()
        classifieur.summary()
        encoder.save_weights('encoder2-2987-weights')
        return encoder


    def build_test_init_module(autoencoder1, encoder2):
        inputs_autoencoder_1 = Input(shape=(64, 64, 1,))
        autoencoder_1_outputs = get_autoencoder1()(inputs_autoencoder_1)
        encoder_2_outputs = encoder2(autoencoder_1_outputs)
        model = Model(inputs=inputs_autoencoder_1, outputs=[autoencoder_1_outputs, encoder_2_outputs])
        model.summary()
        return model

    # encoder2 = get_encoder()
    autoencoder1 = get_autoencoder1()
    autoencoder2 = get_autoencoder2()
    # model = build_test_init_module(autoencoder1, encoder2)

    generateurs, steps = init_auto_generator(params_in_darkmood, params_out_darkmood)
    (X, Y), Xr = generateurs.__next__()

    autoencoder1_out = autoencoder1.predict_on_batch([X[0:3, :, :, :]])
    autoencoder2_out = autoencoder2.predict_on_batch([X[0:3, :, :, :]])

    inputs_autoencoder_1 = Input(shape=(64, 64, 1,))
    autoencoder_1_outputs = autoencoder1(inputs_autoencoder_1)
    autoencoder_2_outputs = autoencoder2(autoencoder_1_outputs)
    model = Model(inputs=inputs_autoencoder_1, outputs=[autoencoder_1_outputs, autoencoder_2_outputs])

    [_, model_out] = model.predict_on_batch([X[0:3, :, :, :]])

    for i in range(3):
        fig = plt.figure(figsize=(8, 8))
        fig.add_subplot(221)
        plt.title("Entrée")
        plt.imshow(X[i, :, :, 0])
        fig.add_subplot(222)
        plt.title("Sortie A1 (image générée)")
        plt.imshow(autoencoder1_out[i, :, :, 0])
        fig.add_subplot(223)
        plt.title("Sortie A2 (input)")
        plt.imshow(autoencoder2_out[i, :, :, 0])
        fig.add_subplot(224)
        plt.title("Sortie A1 + A2 (image générée reconstruite)")
        plt.imshow(model_out[i, :, :, 0])
        plt.show()

    # inputs_autoencoder_1 = Input(shape=(64, 64, 1,))
    # autoencoder_1_outputs = get_autoencoder()(inputs_autoencoder_1)
    # encoder_2_outputs = encoder(autoencoder_1_outputs)
    # model = Model(inputs=inputs_autoencoder_1, outputs=[encoder_2_outputs])
    # model.summary()
