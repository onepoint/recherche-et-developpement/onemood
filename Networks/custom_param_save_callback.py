from keras.callbacks import Callback
import csv
from time import time
import os


class custom_param_save_callback(Callback):
    def __init__(self, logger, save_path, filename="save.csv", separator=';', append=False, freq_loss=1, freq_checkpoint=1, save_weigth=False):
        """
        Création d'un custom callback héritant de la classe callback pour un suivi de l'apprentissage
        Enregistrement du modèle
        :param logger:
        :param filename:
        :param separator:
        :param append:
        :param freq:
        """
        self.sep = separator
        self.filename = filename
        self.append = append
        self.writer_batch = None
        self.writer_epoch = None
        self.csv_file_epoch_loss = open(os.path.join('Save', 'logs', 'epoch_'+filename), 'w')
        self.csv_file = open(os.path.join('Save', 'logs', filename), 'w')
        self.logger = logger
        self.time_training = 0
        self.time_epoch = 0
        self.epoch = 0
        self.freq_loss = freq_loss
        self.freq_checkpoint = freq_checkpoint
        self.save_weight = save_weigth
        self.save_path = save_path
        super(custom_param_save_callback, self).__init__()

    def on_train_begin(self, logs=None):
        """
        Methode de préparation des fichiers de logs au début de l'entraînement
        :param logs:
        :return:
        """
        self.time_training = time()
        self.logger.info('Debut de lentrainement')
        fnames = ['epoch', 'batch', 'size', 'acc', 'loss']
        fnames_epoch = ['epoch', 'acc', 'loss', 'val_acc', 'val_loss']
        self.writer_batch = csv.DictWriter(self.csv_file, fieldnames=fnames, delimiter=';')
        self.writer_batch.writeheader()
        self.writer_epoch = csv.DictWriter(self.csv_file_epoch_loss, fieldnames=fnames_epoch, delimiter=';')
        self.writer_epoch.writeheader()

    def on_epoch_begin(self, epoch, logs=None):
        """
        Logs dans les fichiers de monitoring qu'une epoch débute
        :param epoch:
        :param logs:
        :return:
        """
        self.epoch = epoch
        self.time_epoch = time()
        self.logger.info('Entrainement: epoch {}'.format(epoch))

    def on_epoch_end(self, epoch, logs=None):
        """
        Logs dans le fichier de monitoring qu'une epoch finit
        :param epoch:
        :param logs:
        :return:
        """
        logs = logs or {}
        e = {'epoch': self.epoch}
        res = dict(e, **logs)
        self.writer_epoch.writerow(res)
        duree = abs(self.time_epoch - time())
        self.logger.info('Entrainement: epoch {} terminée, durée: {}'.format(epoch, duree))
        if epoch % self.freq_checkpoint == 0:
            if self.save_weight:
                self.model.save_weights(os.path.join('Save', 'model', self.save_path, 'train-{}'.format(epoch)))
            else:
                self.model.save(os.path.join('Save', 'model', self.save_path, 'train-{}'.format(epoch)))

    def on_batch_end(self, batch, logs=None):
        """
            Suivi selon une fréquence définie par le constructeur des métrics
        """
        if batch % self.freq_loss == 0:
            logs = logs or {}
            epoch = {'epoch': self.epoch}
            res = dict(epoch, **logs)
            self.writer_batch.writerow(res)

    def on_train_end(self, logs=None):
        """
        Fermeture des fichiers csv, écriture dans le fichier de logs de la fin de l'entraînement
        :param logs:
        :return:
        """
        duree = abs(self.time_training - time())
        self.csv_file.close()
        self.csv_file_epoch_loss.close()
        self.writer_batch = None
        self.writer_epoch = None
        self.logger.info('Fin de l\'entraînement à {}'.format(duree))
