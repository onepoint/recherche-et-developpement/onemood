from keras.models import Sequential
from keras.layers import Dense, Flatten, Dropout


class Discriminator:

    def __init__(self):
        model = Sequential()
        model.add(Dropout(rate=0.5))
        model.add(Flatten())
        model.add(Dense(384))
        model.add(Dense(7, activation="softmax"))
        self.model = model
