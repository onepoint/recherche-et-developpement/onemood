from keras.models import Sequential
from keras.layers import Conv2D, BatchNormalization, ReLU, UpSampling2D


class Decoder:

    def __init__(self, input_shape, verbose=False):
        model = Sequential()
        model.add(UpSampling2D(2, interpolation='nearest', input_shape=input_shape))
        model.add(Conv2D(64, 3, padding='same'))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        model.add(UpSampling2D(2, interpolation='nearest'))
        model.add(Conv2D(32, 3, padding='same'))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        model.add(UpSampling2D(2, interpolation='nearest'))
        model.add(Conv2D(1, 5, padding='same'))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        self.model = model
        if verbose:
            self.sum = model.summary()

