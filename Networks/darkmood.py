from keras.models import Model
from keras import backend as K
from keras import optimizers
import os
from Networks.Autoencoders.encoder import Encoder
from Networks.Autoencoders.decoder import Decoder
from Networks.Discriminator.discriminator import Discriminator
from numpy import linalg
from keras.layers import Input, BatchNormalization
import numpy as np
import matplotlib.pyplot as plt
from keras.utils import plot_model
from keras import losses
import tensorflow as tf

# @ TODO faire un script pour la creation de dossiers
# @ TODO ajouter le jeu de validation
# @ TODO vérification de la mise à jour des poids pendant l'apprentissage

class DarkMood:
    def __init__(self, tensorboard, batch_size, steps_per_epoch, logger, learning_rate,
                 dict_emotion, model_type_init="init"):
        """
        DarkMood est le modèle complet assemblant les différents composants de l'architecture:
        - 3 sous modèles sont à considérer:
            - Une partie assemblant 2 autoencoders : autoencoder1_autoencder2_ref
            - Une pertie composée d'un seul autoencoder : auteoncoder_2_in
                    Rq: Ces deux modèles sont composés de deux parties siamoises (en d'autres termes, le même réseau
                    est engagé dans les deux modèles mais des inputs différents lui sont fournis). Une perte est
                    calculée pour ce réseau grâce à ces deux entrées (perte de contraste entre les représentations)
            - Une partie chargée de la classification des émotions présentes en fonction de la sortie de la partie
            précédente.
        Les sous-parties du modèle sont des champs de l'objet DarkMood (attention dans le constructeur, les tensors de
        sortie des sous parties ne sont pas des champs (il font cependant partie du graph construit par Keras et donc
        accessible au besoin)).
        :param steps_per_epoch Nombre de batch par epoch:
        :param logger le logger utilisé pour afficher dans la console ET dans un fichier de log:
        :param imgs_test_auto Des images de référence pour le suivi de l'apprentissage:
        :param learning_rate le coefficient d'apprentissage que l'on peut fixer depuis l'exterieur:
        :param model_type_init mot clef permettant de cibler les modèles à charger lors de l'initilisation:
        """
        self.tensorboard = tensorboard
        self.batch_size = batch_size
        self.steps_per_epoch = steps_per_epoch
        self.logger = logger
        self.learning_rate = learning_rate
        self.input_img_test = []
        self.ref_img_test = []
        self.input_target_test = []
        self.optimizer_darkmood = optimizers.Adam(lr=self.learning_rate, beta_1=0.9, beta_2=0.999, decay=0.0001)
        self.current_target = None
        self.mask1 = np.ones((8, 8, 96), dtype='float32')
        self.mask2 = np.ones((8, 8, 96), dtype='float32')
        self.dict_emotions = dict_emotion
        self.total_loss = []
        self.L_gen = []
        self.L_recons_input = []
        self.L_recons_ref = []
        self.L_class = []
        self.L_contr = []
        self.accuracy_test = []
        self.accuracy_train = []

        # Première chaîne assemblant 2 autoencoders:
        self.inputs_autoencoder_1 = Input(shape=(64, 64, 1,))
        self.encoder_1_ref = Encoder((64, 64, 1), verbose=False).model
        self.encoder_1_ref.name = 'encoder_1_ref'
        self.encoder_1_ref.load_weights(os.path.join('Save', 'model', 'darkmood', 'init', 'encoder1-1400-weights'))
        output_encoder_1_ref = self.encoder_1_ref(self.inputs_autoencoder_1)

        self.decoder_1_ref = Decoder((8, 8, 96), verbose=False).model
        self.decoder_1_ref.name = 'decoder_1_ref'
        self.decoder_1_ref.load_weights(os.path.join('Save', 'model', 'darkmood', 'init', 'decoder1-1400-weights'))
        self.output_decoder_1_ref = self.decoder_1_ref(output_encoder_1_ref)

        self.encoder_2 = Encoder((64, 64, 1), verbose=False).model
        self.encoder_2.name = 'encoder_2'
        self.encoder_2.load_weights(os.path.join('Save', 'model', 'darkmood', 'init', 'encoder2-2987-weights'))
        self.encoder_2.trainable = False

        self.output_encoder_2_ref = self.encoder_2(self.output_decoder_1_ref)
        self.decoder_2_ref = Decoder((8, 8, 96), verbose=False).model
        self.decoder_2_ref.name = 'decoder_2_ref'
        self.decoder_2_ref.load_weights(os.path.join('Save', 'model', 'darkmood', 'init', 'decoder2-1800-weights'))
        output_decoder_2_ref = self.decoder_2_ref(self.output_encoder_2_ref)

        self.autoencoder1_autoencoder2_ref = Model(inputs=self.inputs_autoencoder_1, outputs=[self.output_decoder_1_ref,
                                                                                              self.output_encoder_2_ref,
                                                                                              output_decoder_2_ref])
        # self.autoencoder1_autoencoder2_ref.summary()
        self.autoencoder1_autoencoder2_ref.compile(optimizer=self.optimizer_darkmood,
                                                   loss={'decoder_1_ref': self.custom_loss_recons(1),
                                                         'decoder_2_ref': self.custom_loss_recons(0.25)},
                                                   metrics=['acc'])
        plot_model(self.autoencoder1_autoencoder2_ref, to_file="auto1-auto2.png", show_shapes=True,
                   show_layer_names=True)

        # Seconde chaîne partageant l'encoder_2
        self.input_mask1 = Input(shape=(8, 8, 96,))
        self.input_mask2 = Input(shape=(8, 8, 96,))
        self.inputs_autoencoder_2_in = Input(shape=(64, 64, 1,))
        self.encoder_2.trainable = True
        self.output_encoder_2_in = self.encoder_2(self.inputs_autoencoder_2_in)
        self.encoder_2.load_weights(os.path.join('Save', 'model', 'darkmood', 'init', 'encoder2-2987-weights'))

        self.decoder_2_in = Decoder((8, 8, 96), verbose=False).model
        self.decoder_2_in.name = 'decoder_2_in'

        output_decoder_2_in = self.decoder_2_in(self.output_encoder_2_in)

        self.autoencoder_2_in = Model(inputs=[self.inputs_autoencoder_2_in, self.input_mask1, self.input_mask2],
                                      outputs=[self.output_encoder_2_in, output_decoder_2_in])
        self.autoencoder_2_in.compile(optimizer=self.optimizer_darkmood,
                                      loss={'encoder_2': self.custom_loss_contraste(self.mask1, self.mask2),
                                            'decoder_2_in': self.custom_loss_recons(poids=0.25)})
        plot_model(self.autoencoder_2_in, to_file="auto2-ref.png", show_shapes=True, show_layer_names=True)

        # Troisième chaîne capable de faire de la classification
        self.inputs_darkmood = Input(shape=(8, 8, 96,))
        self.discriminator = Discriminator().model
        self.final_outputs = self.discriminator(self.inputs_darkmood)
        self.darkmood = Model(inputs=self.inputs_darkmood, outputs=self.final_outputs)
        self.darkmood.compile(optimizer=self.optimizer_darkmood, loss=losses.categorical_crossentropy, metrics=['acc'])
        # Initialisation de l'encoder2 seul pour apprentissage
        self.logger.info("Modèles initialisés avec succès.")

    def custom_loss_recons(self, poids=1.0):
        """
        :param poids:
        :return:
        """
        def function_loss(y_true, y_pred):
            delta = y_true - y_pred
            loss = poids * (1 / 2) * (K.sqrt(K.sum(K.square(delta))))
            return loss
        return function_loss

    def create_masks(self, target):
        # mask1 correspond au mask pour conserver les blocs de labels différents
        mask1 = np.ones((target.shape[0], 8, 8, 96))
        # mask2 correspond au mask de même label
        mask2 = np.ones((target.shape[0], 8, 8, 96))
        if target is not None:
            for i in range(target.shape[0]):
                if np.argmax(target[i, :, :]) == 0:
                    mask1[i, :, :, :] = np.zeros((8, 8, 96))
            # mask2 correpond au mask pour conserver les blocs de même label
            for i in range(target.shape[0]):
                if np.argmax(target[i, :, :]) != 0:
                    mask2[i, :, :, :] = np.zeros((8, 8, 96))
        return mask1, mask2

    def custom_loss_contraste(self, mask1, mask2):
        """
        Fonction de perte pour le contraste chargée d'adapter la distance entre les représentations créées:
            - Les images possédant le même label doivent être proche (distance petite)
            - Les images de labels différents doivent être éloignées (distance grande)
        :return current_target:
        """
        mask1 = mask1
        mask2 = mask2

        def function_loss(y_true, y_pred):
            """
            Fonction n'utilisant pas le paramètre y_true car optimisation de la distance entre 2 images prédites
            :param y_true:
            :param y_pred:
            :return:
            """
            margin = K.variable(0.5, dtype=tf.float32)
            loss = K.variable(0, dtype=tf.float32)
            delta_squarred = tf.math.squared_difference(y_true, y_pred)
            # en multipliant par le premier mask on enlève la participation dans la perte
            masked1_delta_squarred = tf.multiply(mask1, delta_squarred)
            masked2_delta_squarred = tf.multiply(mask2, delta_squarred)
            loss_diff_labels = K.sqrt(K.sum(masked1_delta_squarred))
            loss_same_labels = K.maximum(0.0, margin - K.sqrt(K.sum(masked2_delta_squarred)))
            loss = (1/2)*loss_diff_labels + (1/2)*loss_same_labels
            return loss / mask1.shape[0]
        return function_loss

    def normalized_distance(self, input, reference):
        input_size = linalg.norm(input)
        reference_size = linalg.norm(reference)
        normalized_input = input / input_size
        normalized_reference = reference / reference_size
        contrastive_representation = np.absolute(normalized_input - normalized_reference)
        return contrastive_representation

    def train(self, generator, epochs):
        """
        Méthode pour l"entraînement de darkmmod. Les étapes suivies sont:
        - Génération d'un batch d'images de référence / calcul de la perte L_gen / rétropropagation de cette perte
        - Passage du batch de référence et du batch en input dans l'encoder2 et calcul de la perte Lcons
        - Passage des représentations dans les 2 decoders (attention pas de partage des paramètres) et calcul de Lrecons
        - Calcul de la distance entre les batches générés
        - Passage de ce vecteur de distance dand darkmood qui classifie
        :param generator Generateur capable de renvoyer un tuple (batch entree, batch target) et un batch de reference:
        :param epochs :
        :return:
        """
        for i in range(epochs):
            for steps in range(self.steps_per_epoch+1):
                # Génération d'un batch pour l'époch courante:
                # (Xtrain, self.current_target), Xr = generator.__next__()
                (train, test) = generator.__next__()
                (Xtrain, self.current_target, Xref_train) = train
                (Xtest,  Ytest, Xref_test) = test
                self.current_target = np.expand_dims(self.current_target, axis=2)
                Ytest = np.expand_dims(Ytest, axis=2)
                (self.mask1, self.mask2) = self.create_masks(self.current_target)
                (mask1_test, mask2_test) = self.create_masks(Ytest)
                # On fait toutes les vérifications lors de la première époch et du premier batch
                if steps == 0 and i == 0:
                    # On vérifie bien que les batchs générés sont du bon format
                    self.logger.info("X.shape = {}, Xr.shape = {}, Target.shape = {}".format(Xtrain.shape,
                                                                                             Xref_train.shape,
                                                                                             self.current_target.shape))
                    self.logger.info("Xtest.shape = {}, Xreftest.shape = {}, Targetref.shape = {}".format(Xtest.shape,
                                                                                                          Xref_test.shape,
                                                                                                          Ytest.shape))
                    # On conserve aussi les 3 premières images du premier batch afin d'avoir une référence pour le suivi
                    for j in range(3):
                        self.input_img_test.append(Xtrain[j, :, :, 0])
                        self.input_target_test.append(self.current_target[i, :])
                        plt.imshow(Xtrain[j, :, :, 0])
                        plt.title("Image d'entrée: ({})".
                                  format(self.dict_emotions[str(np.argmax(self.current_target[0, :]))]))
                        plt.savefig(os.path.join("Save", "outputs_imgs", "darkmood", "{}".format(j), "input.png"))
                        self.ref_img_test.append(Xref_train[j, :, :, 0])
                        plt.imshow(Xref_train[j, :, :, 0])
                        plt.title("Image référence (sans émotion)")
                        plt.savefig(os.path.join("Save", "outputs_imgs", "darkmood", "{}".format(j), "ref.png"))
                    # Génération d'images pour voir si les modèles génératifs fonctionnent correctement:
                    self.follow_the_training(0, 0)

                # Phase 1:
                target_dict_ref = {'decoder_1_ref': Xref_train, 'decoder_2_ref': Xtrain}
                # Première pass dans autoencoder1 et decoder2 pour création de l'image de référence
                [self.output_decoder_1_ref, self.output_encoder_2_ref, _] = self.autoencoder1_autoencoder2_ref.\
                    predict_on_batch(Xtrain)
                [_, output_encoder_2_ref_test, _] = self.autoencoder1_autoencoder2_ref. predict_on_batch(Xtest)
                # Après génération des images en forward pass on rétropropage Lgen et Lrecons_ref:
                # Normalement cette perte NE DOIT pas être rétropropagée dans l'encoder 2, on le vérifie:
                # encoder2_before = clone_model(self.autoencoder1_autoencoder2_ref.get_layer(name='encoder_2'))
                # self.autoencoder1_autoencoder2_ref.get_layer(name='encoder_2').save_weights('encoder2_before-1')
                # encoder2_before.load_weights('encoder2_before-1')
                loss_pass1 = self.autoencoder1_autoencoder2_ref.train_on_batch(Xtrain, target_dict_ref)
                # On enregistre la perte au cours du temps:
                self.L_gen.append(loss_pass1[1])
                self.L_recons_ref.append(loss_pass1[2])
                # encoder2_after = self.autoencoder1_autoencoder2_ref.get_layer(name='encoder_2')
                # Pour les phases de test on laisse cette vérification pour tous les batchs:
                # if not self.networks_comparaison(encoder2_before, encoder2_after, plot=False):
                #    self.logger.info("Après entraînement (pass 1) l'encoder2 ne s'est pas modifié.")

                # Phase 2:
                # Pass dans l'autoencoder 2 de l'image d'entrée pour génération de la représentation de l'encoder2
                # Pour l'encoder, on propose en cible la sortie de l'encoder2 pour l'image générée (dans la perte ce
                # sera le paramétre "y_true"
                target_dict_in = {'decoder_2_in': Xtrain, 'encoder_2': self.output_encoder_2_ref}
                [output_encoder_2_in_test, _] = self.autoencoder_2_in.predict_on_batch([Xtest, mask1_test, mask2_test])
                [self.output_encoder_2_in, _] = self.autoencoder_2_in.predict_on_batch([Xtrain, self.mask1, self.mask2])
                # Apprentissage sur l'autoencoder2
                # encoder2_before = clone_model(self.autoencoder_2_in.get_layer(name='encoder_2'))
                # self.autoencoder_2_in.get_layer(name='encoder_2').save_weights('encoder2_before-2')
                # encoder2_before.load_weights('encoder2_before-2')
                loss_pass2 = self.autoencoder_2_in.train_on_batch([Xtrain, self.mask1, self.mask2], target_dict_in)
                self.L_contr.append(loss_pass2[1])
                self.L_recons_input.append(loss_pass2[2])
                # if self.networks_comparaison(encoder2_before, self.autoencoder_2_in.get_layer(name='encoder_2')):
                #    self.logger.info("Après entraînement (im input) l'encoder2 s'est modifié.")

                # Phase 3:
                # Calcul du vecteur de contraste
                delta = self.normalized_distance(self.output_encoder_2_in, self.output_encoder_2_ref)
                delta_test = self.normalized_distance(output_encoder_2_in_test, output_encoder_2_ref_test)
                # On verifie que ce vecteur est de la bonne taille:
                if delta.shape != self.output_encoder_2_in.shape:
                    self.logger.warn(delta.shape)
                self.current_target = np.squeeze(self.current_target, axis=2)
                Ytest = np.squeeze(Ytest)
                class_loss = self.darkmood.train_on_batch(delta, self.current_target)
                self.accuracy_test.append(self.darkmood.test_on_batch(delta_test, Ytest)[1])
                self.L_class.append(class_loss[0])
                total_loss = loss_pass1[0] + loss_pass2[0] + class_loss[0]
                self.total_loss.append(total_loss)
                self.logger.info("Epoch: {}. Batch numéro: {}. Perte total: {}".format(i, steps, total_loss))

                if steps % 5 == 0:
                    # Tous les 3 batchs on enregistre une verif des imgs de sortie + modeles
                    self.follow_the_training(step=steps, epoch=i)
                    self.save_accuracy(i)

            # Toutes les 5 epochs on enregistre les graphs de perte
            if i == 0 or i == 1 or i % 5 == 0:
                self.save_loss(i)
                self.save_accuracy(i)
        return

    def save_accuracy(self, epochs):
        # time = np.linspace(0, len(self.accuracy_test), len(self.accuracy_test))
        # print(self.accuracy_test)
        plt.figure()
        plt.plot(self.accuracy_test)
        plt.title("Précision sur jeu de validation")
        plt.title(str(epochs))
        plt.savefig(os.path.join("Save", "outputs_imgs", "darkmood", "Suivi-précision-e{}".format(epochs)))
        np.save('accuracy', np.array(self.accuracy_test))
        plt.close()
        return

    def save_loss(self, epochs):
        plt.figure()
        time = np.linspace(0, len(self.total_loss), len(self.total_loss))

        fig = plt.figure(figsize=(18, 10))
        fig.add_subplot(231)
        plt.title("Perte totale")
        plt.plot(time[1:], self.total_loss[1:])

        fig.add_subplot(232)
        plt.title("Perte de reconstruction du modele generatif (Lgen)")
        plt.plot(time[1:], self.L_gen[1:])

        fig.add_subplot(233)
        plt.title("Perte de reconstruction de l'autoencoder2 (Lrecons_ref)")
        plt.plot(time[1:], self.L_recons_ref[1:])

        fig.add_subplot(234)
        plt.title("Perte de reconstruction de l'autoencoder2 (Lrecons_in)")
        plt.plot(time[1:], self.L_recons_input[1:])

        fig.add_subplot(235)
        plt.title("Perte de contraste (L_contr)")
        plt.plot(time[1:], self.L_contr[1:])

        fig.add_subplot(236)
        plt.title("Perte de classification (Lclass)")
        plt.plot(time[1:], self.L_class[1:])
        plt.savefig(os.path.join("Save", "outputs_imgs", "darkmood", "Suivi-loss-e{}".format(epochs)))
        np.save('losses', np.array([self.total_loss, self.L_gen, self.L_recons_ref,
                                    self.L_recons_input, self.L_contr, self.L_class]))
        plt.close()
        return

    def follow_the_training(self, step, epoch):
        """
        Quand cette méthode est appelée, elle produit une pass à travers tout le réseau d'un batch constitué des
        3 images de référence
        :return:
        """
        # @TODO multiplier les subplots des représentations encodees (96 disponibles)
        X = np.array(self.input_img_test).reshape((3, 64, 64, 1))
        [decoder1, encoder2_ref, decoder2_ref] = self.autoencoder1_autoencoder2_ref.predict_on_batch(X)
        [encoder2_in, decoder2_in] = self.autoencoder_2_in.predict_on_batch([X, self.mask1, self.mask2])
        for i in range(3):
            fig = plt.figure(figsize=(12, 10))
            fig.add_subplot(221)
            plt.title("Image d'entrée")
            plt.imshow(X[i, :, :, 0])
            fig.add_subplot(222)
            plt.title("Image de référence générée")
            plt.imshow(decoder1[i, :, :, 0])
            # fig.add_subplot(233)
            # plt.title("Représentation de l'image de référence générée")
            # plt.imshow(encoder2_ref[i, :, :, 0])
            fig.add_subplot(224)
            plt.title("Reconstruction de l'image de référence générée")
            plt.imshow(decoder2_ref[i, :, :, 0])
            fig.add_subplot(223)
            # plt.title("Représentation de l'image d'entrée")
            # plt.imshow(encoder2_in[i, :, :, 0])
            # fig.add_subplot(236)
            plt.title("Reconstruction de l'image d'entrée")
            plt.imshow(decoder2_in[i, :, :, 0])
            plt.savefig(os.path.join("Save", "outputs_imgs", "darkmood", "{}".format(i),
                                     "Suivi-batch_num{}-epoch_num{}".format(step, epoch)))
            plt.close(fig)
        # Sauvegarde des modèles:
        self.darkmood.save(os.path.join("Save", "model", "darkmood", "darkmood-batch_{}-epoch_{}".format(step, epoch)))
        self.autoencoder1_autoencoder2_ref.save(os.path.join("Save", "model", "darkmood", "ref-batch_{}-epoch_{}".format(step, epoch)))
        self.autoencoder_2_in.save(os.path.join("Save", "model", "darkmood", "in-batch_{}-epoch_{}".format(step, epoch)))
        return

    def networks_comparaison(self, model_1, model_2, plot=False):
        """
        Méthode permettant la comparaison des poids de deux modèles (utile pour voir si il y a eu un apprentissage
            Notons que la batch normalization possède 4 paramètres [gamma, beta, mean, variance]
        :param model_1:
        :param model_2:
        :param plot:
        :return:
        """
        if plot:
            plot_model(model_1, to_file=str(model_1), show_shapes=True, show_layer_names=True)
            plot_model(model_2, to_file=str(model_2), show_shapes=True, show_layer_names=True)

        # Variable de vérité pour la comparaison des modèles
        same = False
        if len(model_1.layers) == len(model_2.layers):
            for layer_m1, layer_m2 in zip(model_1.layers, model_2.layers):
                # Si les couches ont le même nombre de type de paramètres et si elles possèdent des paramètres:
                if len(layer_m1.get_weights()) == len(layer_m2.get_weights()):
                    # Si le nombre de paramètres sur ces couche n'est pas nulle
                    if len(layer_m2.get_weights()) > 0:
                        # Si ce n'est pas une couche de batchnormalization
                        if not isinstance(layer_m2, BatchNormalization):
                            for param1, param2 in zip(layer_m1.get_weights(), layer_m2.get_weights()):
                                # Si tous les paramètres sont égaux la variable de vérité sortira à True!
                                same = np.all(param1 == param2)
                                if not same:
                                    # Si les paramètres ne sont pas égaux, erreur fatale!
                                    # On sort avec une égalité fausse!
                                    # print(param1[0, 0], param2[0, 0])
                                    # print(param1 == param2)
                                    # print(param1 - param2)
                                    self.logger.info("Paramètres non identiques identifiées sur les couches {}/{}".
                                                     format(str(layer_m1.name), str(layer_m2.name)))
                                    return same
                else:
                    # La seule erreur est fatale, ici pas le même nombre de noeuds / biais
                    self.logger.info("Nombre de paramètre : {} vs {}".format(len(layer_m1.get_weights()),
                                                                             len(layer_m2.get_weights())))
                    same = False
                    return same
        else:
            # Une nouvelle fois l'erreur est fatale! Si les modèles n'ont pas le même nombre de couches
            self.logger.info("Pas des couches de même dimension, model1 a {} couches contre {} pour le model2!".
                             format(len(model_1.layers), len(model_2.layers)))
            same = False
            return same
        # Dans le cas où tous les pamamètres sont égaux, on retourne True
        self.logger.info("Les deux modèles sont parfaitement identiques")
        return same

    def predict(self, model, imgs):
        """
        Predicition sur un batch passé en paramètre (surtout pour l'accés depuis l'ext)
        Sert surement à rien
        :param imgs:
        :param model
        :return:
        """
        # a utiliser
        batch_output = model.predict_on_batch(imgs)
        return batch_output

