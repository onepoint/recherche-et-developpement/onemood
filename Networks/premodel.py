from keras.models import Sequential, load_model
from keras import optimizers
from keras import losses
import matplotlib.pyplot as plt
import os
import numpy as np
from Networks.Autoencoders.encoder import Encoder
from Networks.Autoencoders.decoder import Decoder
from Networks.Discriminator.discriminator import Discriminator
from keras.callbacks import EarlyStopping
import cv2 as cv
from Networks.custom_param_save_callback import custom_param_save_callback
from Networks.custom_photo_param_save_callback import custom_photo_param_save_callback


class PreModel:
    """
    Classe pour pré-entraîner les modèles :
    Instancier un objet de cette classe revient à initialiser deux modèles : un discriminateur et un autoencoder
    """
    def __init__(self, imgs_test_auto, logger, verbose=True, model="init"):
        """
        Constructeur pour entraîner des pré-models
        Plusieurs possibiltés de construction: soit à partir de rien model = "init", soit à partir de models sauvegardes
        (à préfixer par un a pour un autoencoder)
        Notons qu'il n'est pas possible de charger 2 modèles en même temps (on a donc toujours un auto ou un disc qui
        est initialise par défaut alors même qu'il n'est pas utilisé)
        :param imgs_test_auto:
        :param logger:
        :param verbose:
        :param model:
        """
        self.logger = logger
        self.imgs_test = imgs_test_auto
        self.model_discriminator = Sequential()
        self.model_autoencoder = Sequential()
        # Cas ou créé l'objet en fonction d'un modèle d'autoencoder déjà entraîné
        if model == "init":
            # Cas ou on initialise un autoencoder ou un discriminator
            # on ne fera jamais
            self.init_auto()
            self.init_disc()
        else:
            # Si l'on souhaite charger un modèle:
            if model[0] == 'a':
                # Les modèles des autoencoders sont préfixés par un 'a'
                model_path = os.path.join('Save', 'model', 'autoencoder1', model)
                model = load_model(model_path)
                self.model_autoencoder = model
                self.logger.info('Autoencoder charge avec succes')
                # On ne chargera jamais deux modèles mais on init quand même l'autre
                self.init_disc()
            else:
                model_path = os.path.join('Save', 'model', 'discriminator', model)
                model = load_model(model_path)
                self.model_discriminator = model
                self.logger.info('Discriminator charge avec succes')
                self.init_auto()

        # Dans tous les cas on init un optimizer et les imgs_tests
        # expliquer les params
        self.optimizer_autoencoder = optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, decay=0.0001)
        self.optimizer_discriminator = optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, decay=0.0001)
        # On compile l'autoencoder
        self.model_autoencoder.compile(optimizer=self.optimizer_autoencoder, loss=losses.mean_squared_error,
                                       metrics=['acc'])
        self.model_discriminator.compile(optimizer=self.optimizer_discriminator, loss=losses.categorical_crossentropy,
                                         metrics=['acc'])
        if verbose:
            self.model_autoencoder.summary()
            self.model_discriminator.summary()

    def init_disc(self):
        """
        Initialisation d'un discriminateur composé de 3 couches de convolutions et 2 couches denses
        Méthode pour éviter la copie de code
        :return:
        """
        model = Sequential()
        # params dans un fichier de config
        encoder = Encoder((64, 64, 1), verbose=False)
        discriminator = Discriminator()
        model.add(encoder.model)
        model.add(discriminator.model)
        self.model_discriminator = model
        self.logger.info('Discriminateur initialise avec succes')
        print('Discriminateur initialise avec succes')

    def init_auto(self):
        """
        Initialisation d'un autoencoder composé de 2 CNN (encoder puis decoder)
        Méthode pour éviter la copie de code
        :return:
        """
        encoder = Encoder((64, 64, 1), verbose=False)
        decoder = Decoder((8, 8, 96), verbose=False)
        model = Sequential()
        model.add(encoder.model)
        model.add(decoder.model)
        self.model_autoencoder = model
        self.logger.info('Autoencoder initialise avec succes')
        print('Autoencoder initialise avec succes')

    def discriminator_train(self, train_generator, test_generator, steps_per_epoch, validation_steps,
                            epochs, verbose=1):
        """
        Méthode de pré-entrainement du baseline CNN
        :param train_generator:
        :param validation_steps:
        :param test_generator:
        :param steps_per_epoch:
        :param epochs:
        :param verbose:
        :return:
        """
        early_stop = EarlyStopping(monitor='accuracy', mode='max', verbose=1, patience=0)
        custom_callback = custom_param_save_callback(self.logger, save_path="discriminator")
        history = self.model_discriminator.fit_generator(train_generator, max_queue_size=100,
                                                         steps_per_epoch=steps_per_epoch,
                                                         use_multiprocessing=False, workers=16, epochs=epochs,
                                                         validation_data=test_generator,
                                                         validation_steps=validation_steps,
                                                         verbose=verbose, callbacks=[custom_callback])
        # evaluated_last_hist = self.model_discriminator.evaluate_generator(test_generator, steps=1)
        return history

    def autoencoder_train(self, train_generator, steps_per_epoch, epochs, verbose=1, recons=True):
        """
        Méthode de pré-entrainement des auto-encoders:
        - Enregistrement des images de référence
        :param train_generator:
        :param steps_per_epoch:
        :param epochs:
        :param verbose:
        :param recons:
        :return:
        """
        for i in range(0, 3):
            plt.imshow(self.imgs_test[i, :, :, 0], cmap='gray')
            if recons:
                plt.savefig('Save/outputs_imgs/autoencoder2/{}/imsreferences_{}'.format(str(i), str(i)))
            else:
                plt.savefig('Save/outputs_imgs/autoencoder1/{}/imsreferences_{}'.format(str(i), str(i)))
        self.logger.info('Images de tests pretes pour l entrainement')
        early_stop = EarlyStopping(monitor='acc', mode='auto', verbose=1, patience=1000)
        custom_callback_gen = custom_photo_param_save_callback(self.logger, save_path="autoencoder",
                                                               recons=recons, imgs_test=self.imgs_test)
        print(steps_per_epoch)
        history = self.model_autoencoder.fit_generator(train_generator, max_queue_size=100,
                                                       steps_per_epoch=steps_per_epoch,
                                                       use_multiprocessing=True, workers=16, epochs=epochs,
                                                       verbose=verbose,
                                                       callbacks=[custom_callback_gen, early_stop])
        return history

    def test_video(self):
        """
        Module de test du baseline CNN en conditions "réelles" à l'aide d'un module de détection du visage d'openCV
        :return:
        """
        # Création d'un dictionnaire correspondant au one-hot encoding
        emotions = {
            '1': "Enervement",
            '2': "Neutral",
            '3': "Dégout",
            '4': "Peur",
            '5': "Joie",
            '6': "Tristesse",
            '7': "Surprise"
        }
        cv_path = 'C:\\Users\\m.adolphe\\Desktop\\Stage\\onemood\\Datas\\haarcascade_frontalface_alt.xml'
        cap = cv.VideoCapture(0)
        face_cascade = cv.CascadeClassifier(cv_path)
        while(True):
            # Capture frame-by-frame
            ret, frame = cap.read()
            # Detection des visages:
            faces = face_cascade.detectMultiScale(frame, scaleFactor=1.3, minNeighbors=5)
            for (x, y, w, h) in faces:
                # Pour les coordonnées de la limite des visages détectés, tracer un rectangle et prédire l'émotion
                # présente sur le visage
                cv.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                new = frame[y: y + h, x: x + w]
                new = cv.resize(cv.cvtColor(new, cv.COLOR_BGR2GRAY), (64, 64))
                res = np.argmax(self.model_discriminator.predict(new.reshape(1, 64, 64, 1))) + 1
                cv.putText(img=frame, text=emotions[str(res)], org=(40, 150),
                            fontFace=cv.FONT_HERSHEY_PLAIN, fontScale=3,
                            color=(255, 0, 0))
            # On convertit en niveau de gris puis on affiche l'image
            gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
            # Display the resulting frame
            cv.imshow('frame', gray)
            if cv.waitKey(1) & 0xFF == ord('q'):
                break
