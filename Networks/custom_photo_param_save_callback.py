from keras.callbacks import Callback
import csv
from time import time
import os
from .custom_param_save_callback import custom_param_save_callback
import matplotlib.pyplot as plt
import numpy as np
import keras.backend as K


class custom_photo_param_save_callback(custom_param_save_callback):
    """
    Classe fille de CustomCallback
    """
    def __init__(self, logger, save_path, recons, imgs_test, freq_save=3, freq_checkpoint=3):
        """
        :param logger:
        :param save_path:
        :param recons:
        :param imgs_test:
        :param freq_save:
        """
        custom_param_save_callback.__init__(self, logger=logger, save_path=save_path)
        self.recons = recons
        self.imgs_test = imgs_test
        self.freq_save = freq_save
        self.freq_checkpoint = freq_checkpoint
        self.learning_rates = []

    def on_batch_begin(self, batch, logs=None):
        return self

    def on_train_begin(self, logs=None):
        """
        Methode de préparation des fichiers de logs au début de l'entraînement
        :param logs:
        :return:
        """
        self.time_training = time()
        self.logger.info('Debut de lentrainement')
        fnames = ['epoch', 'batch', 'size', 'loss', 'acc']
        self.writer_batch = csv.DictWriter(self.csv_file, fieldnames=fnames, delimiter=';')
        self.writer_batch.writeheader()

    def on_epoch_end(self, epoch, logs=None):
        """
        Logs dans le fichier de monitoring qu'une epoch finit
        :param epoch:
        :param logs:
        :return:
        """
        logs = logs or {}
        duree = abs(self.time_epoch - time())
        self.logger.info('Entrainement: epoch {} terminée, durée: {}'.format(epoch, duree))
        if True:
            batch_output = self.model.predict_on_batch(self.imgs_test)
            for i in range(3):
                temp = batch_output[i, :, :, 0]
                temp += 0.5
                plt.imshow(temp, cmap='gray')
                if self.recons:
                    plt.savefig('Save/outputs_imgs/autoencoder2/{}/out_{}'.format(str(epoch), str(epoch)))
                else:
                    plt.savefig('Save/outputs_imgs/autoencoder1/{}/out_{}'.format(str(epoch), str(epoch)))
        if epoch % self.freq_checkpoint == 0:
            if self.save_weight:
                self.model.save_weights(os.path.join('Save', 'model', self.save_path, 'a-train-{}'.format(epoch)))
            else:
                self.model.save(os.path.join('Save', 'model', self.save_path, 'a-train-{}'.format(epoch)))

    def on_batch_end(self, batch, logs=None):
        """
            Suivi selon une fréquence définie par le constructeur des métrics
        """
        self.learning_rates.append(K.eval(self.model.optimizer.lr))
        if batch % self.freq_loss == 0:
            logs = logs or {}
            epoch = {'epoch': self.epoch}
            res = dict(epoch, **logs)
            self.writer_batch.writerow(res)

    def on_train_end(self, logs=None):
        """
        Fermeture des fichiers csv, écriture dans le fichier de logs de la fin de l'entraînement
        :param logs:
        :return:
        """
        np.save('lr', np.array(self.learning_rates))
        duree = abs(self.time_training - time())
        self.csv_file.close()
        self.writer_batch = None
        self.logger.info('Fin de l\'entraînement à {}'.format(duree))